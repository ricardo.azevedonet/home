import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/service/app.service';

@Component({
  selector: 'app-colors',
  templateUrl: './colors.component.html',
  styleUrls: ['./colors.component.scss']
})
export class ColorsComponent implements OnInit {

  public isCopied: any;

  constructor(public service: AppService) { }

  ngOnInit() {

  }

  getdata() {
    return this.service.colors;
  }

  loadColors() {

  }

}
