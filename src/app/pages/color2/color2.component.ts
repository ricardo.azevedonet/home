import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/service/app.service';

@Component({
  selector: 'app-color2',
  templateUrl: './color2.component.html',
  styleUrls: ['./color2.component.scss']
})
export class Color2Component implements OnInit {

  public isCopied: any;

  color2 = [];

  constructor(public service: AppService) { }

  ngOnInit() {
    this.newColors();
  }

  newColors() {
    this.color2 = [];
    for (let index = 0; index < 18; index++) {
      this.color2.push({ variations: this.generate() });

    }
  }

  getdata() {
    return this.service.colors2;
  }

  generate() {
    let colors = [];

    let rgb, grey, color;
    for (let i = 1; i < 6; i++) {
      rgb = this.createRGB();
      // grey = this.greyscale(rgb);
      color = this.RGBtoHex(rgb);
      // console.log(color);
      // $("#box_" + i).css("background", "#" + color);
      // // $("#box_" + i).css("background", "rgb(" + rgb + ")");
      // $(".text_" + i).text("#" + color);
      // $(".text_" + i).css("color", "rgb(" + grey + ")");

      colors.push(`#${color}`);
    }
    return colors;
    // $(".container > div").css("box-shadow", "3px 3px 10px #DEE0E0");
  }

  createRGB() {
    let color = [];
    for (let i = 0; i < 3; i++) {
      let random = Math.floor(Math.random() * 256);
      color.push(random);
    }
    return color;
  };

  RGBtoHex(rgb) {
    return rgb.reduce(function (hex, color) {
      const hexLetters = ['A', 'B', 'C', 'D', 'E', 'F'];
      let second = color % 16;
      let first = (color - second) / 16;
      hex += first > 9 ? hexLetters[first - 10] : first;
      hex += second > 9 ? hexLetters[second - 10] : second;
      return hex;
    }, '');
  }

  greyscale(arr) {
    const grey = Math.floor(0.21 * arr[0] + 0.72 * arr[1] + 0.07 * arr[2]);
    return grey < 150 ? [235, 235, 235] : [35, 35, 35];
  }


}
