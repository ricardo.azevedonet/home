import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/service/app.service';

@Component({
  selector: 'app-fontawesome',
  templateUrl: './fontawesome.component.html',
  styleUrls: ['./fontawesome.component.scss']
})
export class FontawesomeComponent implements OnInit {

  public isCopied: any;

  public copyTag: boolean = false;
  public searchText: string;

  constructor(public service: AppService) { }

  ngOnInit() {
  }

  getdata() {
    return this.service.icons;
  }

  copy(value) {
    return this.copyTag ? '<i class="fa fa-' + value + '" aria-hidden="true"></i>' : 'fa-' + value;
  }

  loadIcons() {

  }

}
