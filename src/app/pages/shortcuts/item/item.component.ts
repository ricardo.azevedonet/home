import { Component, OnInit, Input } from '@angular/core';
import { AppService } from 'src/app/service/app.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  contextMenu;
  // isCopied;

  @Input() line;
  @Input() color;
  @Input() groupIndex;
  @Input() itemIndex;

  constructor(public service: AppService) { }

  ngOnInit(): void {

    this.contextMenu = [
      {
        label: `edit ${this.getName()}`,
        icon: 'pi pi-pencil',
        command: (event) => {
          this.service.editItem = {
            type: 'item',
            group: this.groupIndex,
            name: this.line.name,
            code: this.line.code,
            index: this.itemIndex
          };
          //event.originalEvent: Browser event
          //event.item: menuitem metadata
        }
      },
      {
        label: `remove ${this.getName()}`,
        icon: 'pi pi-times',
        command: (event) => {
          this.service.data[this.groupIndex].items.splice(this.itemIndex, 1);
          this.service.saveShortcuts();
        }
      }
    ];



  }

  getName() {
    return this.line.name.substring(0, 8) + '...';
  }

  copy(value: string) {
    let re = /``/gi;
    return value.replace(re, '"');
    // return value;
  }

}
