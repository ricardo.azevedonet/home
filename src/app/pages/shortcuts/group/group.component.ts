import { Component, OnInit, Input } from '@angular/core';
import { AppService } from 'src/app/service/app.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {

  @Input() group;
  @Input() index;
 
  contextMenu;

  constructor(public service:AppService) { }

  ngOnInit(): void {

    this.contextMenu = [
      {
        label: `new item in ${this.getName()}` ,
        icon: 'pi pi-file-o', command: (event) => {
          this.service.editItem = {
            type:'item',
            group:this.index,
            name: '',
            code: ''
          };
        }
      },
      {
        label: `edit group ${this.getName()}` ,
        icon: 'pi pi-pencil', command: (event) => {
          this.service.editItem = {
            type:'group',
            group:this.index,
            title: this.group.title,
            color: this.group.color,
            icon: this.group.icon
          };
        }
      }
    ];

  }

  getName(){
    return this.group.title.substring(0, 5) + '...';
  }

}
