import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShortcutsComponent } from './pages/shortcuts/shortcuts.component';
import { ColorsComponent } from './pages/colors/colors.component';
import { FontawesomeComponent } from './pages/fontawesome/fontawesome.component';
import { Color2Component } from './pages/color2/color2.component';


const routes: Routes = [
  { path: 'shortcuts', component: ShortcutsComponent },
  { path: 'colors', component: ColorsComponent },
  { path: 'colors2', component: Color2Component },
  { path: 'fontawesome', component: FontawesomeComponent },
  { path: 'java', component: ColorsComponent },

  { path: '**', redirectTo: '/shortcuts', pathMatch: 'full' },
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
