import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
// import { map } from 'rxjs/operators';

import { StorageMap } from '@ngx-pwa/local-storage';
// import { Subject } from 'rxjs';

import Shortcuts from '../../assets/shortcuts.json';
import Colors1 from '../../assets/color1.json';
import Icons from '../../assets/icons47.json';
import Colors2 from '../../assets/color2.json';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  data: any;
  colors: any;
  icons: any;

  colors2: any;

  rSidebar: boolean = true;

  isCopied: any;
  editItem: any = null;

  constructor(private http: Http, private storage: StorageMap) {

    // http.get('/assets/color1.json')
    //   .pipe(map(res => res.json()))
    //   .subscribe(data => {
    //     this.colors = data.colors
    //   });

    // http.get('/assets/shortcuts.json')
    //   .pipe(map(res => res.json()))
    //   .subscribe(data => {
    //     // this.data = data;
    //     data.sort(function (a, b) {
    //       return b.items.length - a.items.length;
    //     });
    //     // this.saveObject("shortcuts", this.data);

    //     this.storage.get("shortcuts").subscribe((value) => {
    //       // console.log(data)
    //       // console.log(value)
    //       console.log(this.isEqual(data, value))

    //       if (value && value != undefined) {
    //         this.data = value;
    //       } else {
    //         this.data = data;
    //       }

    //     });

    //   });

    // http.get('/assets/icons47.json')
    //   .pipe(map(res => res.json()))
    //   .subscribe(data => {
    //     this.icons = data.icons
    //   });

    // let users: any = [
    //   { firstName: 'Henri1', lastName: 'Bergson' },
    //   { firstName: 'Henri2', lastName: 'Bergson' },
    //   { firstName: 'Henri3', lastName: 'Bergson' },
    // ];
    // this.storage.set('users', users).subscribe(() => {
    //   console.log("saved()");
    // });



    this.storage.get("shortcuts").subscribe((value) => {
      // console.log(data)
      // console.log(value)
      // console.log(this.isEqual(Shortcuts, value))

      if (value && value != undefined) {
        this.data = this.sortList(value);
      } else {
        this.data = this.sortList(Shortcuts);;
      }

    });

    this.colors = Colors1.colors;
    this.colors2 = Colors2;
    this.icons = Icons.icons;

  }

  sortList(data) {
    return data.sort(function (a, b) {
      return b.items.length - a.items.length;
    });
  }

  //https://github.com/cyrilletuzi/angular-async-local-storage
  saveObject(key: string, value: any) {
    this.storage.set(key, value).subscribe(() => {
      console.log(`${key} saved()!`);
    });
  }


  downloadShortcuts() {
    this.downloadJSON("shortcuts.json", this.data);
  }

  downloadJSON(name: string, value: any) {

    let json: any = JSON.stringify(value);

    //Convert JSON string to BLOB.
    json = [json];
    var blob1 = new Blob(json, { type: "text/json;charset=utf-8" });


    var url = window.URL || window.webkitURL;
    let link = url.createObjectURL(blob1);
    var a = document.createElement("a");
    a.download = name;
    a.href = link;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

  saveShortcuts() {
    this.data = this.sortList(this.data);
    this.saveObject("shortcuts", this.data);
  }

  isEqual(value, other) {

    // Get the value type
    var type = Object.prototype.toString.call(value);

    // If the two objects are not the same type, return false
    if (type !== Object.prototype.toString.call(other)) return false;

    // If items are not an object or array, return false
    if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;

    // Compare the length of the length of the two items
    var valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
    var otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
    if (valueLen !== otherLen) return false;



    // Compare properties
    if (type === '[object Array]') {
      for (var i = 0; i < valueLen; i++) {
        if (this.compare(value[i], other[i]) === false) return false;
      }
    } else {
      for (var key in value) {
        if (value.hasOwnProperty(key)) {
          if (this.compare(value[key], other[key]) === false) return false;
        }
      }
    }

    // If nothing failed, return true
    return true;

  };

  // Compare two items
  compare(item1, item2) {

    // Get the object type
    var itemType = Object.prototype.toString.call(item1);

    // If an object or array, compare recursively
    if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
      if (!this.isEqual(item1, item2)) return false;
    }

    // Otherwise, do a simple comparison
    else {

      // If the two items are not the same type, return false
      if (itemType !== Object.prototype.toString.call(item2)) return false;

      // Else if it's a function, convert to a string and compare
      // Otherwise, just compare
      if (itemType === '[object Function]') {
        if (item1.toString() !== item2.toString()) return false;
      } else {
        if (item1 !== item2) return false;
      }

    }
  };

}
