import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ClipboardModule } from 'ngx-clipboard';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShortcutsComponent } from './pages/shortcuts/shortcuts.component';
import { ColorsComponent } from './pages/colors/colors.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FontawesomeComponent } from './pages/fontawesome/fontawesome.component';
import { SearchPipe } from './pipe/search.pipe';

import { SidebarModule } from 'primeng/sidebar';
import { ContextMenuModule } from 'primeng/contextmenu';
import { GroupComponent } from './pages/shortcuts/group/group.component';
import { ItemComponent } from './pages/shortcuts/item/item.component';
import { Color2Component } from './pages/color2/color2.component';

@NgModule({
  declarations: [
    AppComponent,
    ShortcutsComponent,
    ColorsComponent,
    SidebarComponent,
    FontawesomeComponent,
    SearchPipe,
    GroupComponent,
    ItemComponent,
    Color2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    ClipboardModule,
    HttpModule,
    BrowserAnimationsModule,

    SidebarModule,
    ContextMenuModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
